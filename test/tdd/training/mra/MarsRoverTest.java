package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {

	private MarsRover rover;
	
	@Before
	public void setup() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		this.rover = new MarsRover(10,10,planetObstacles);
	}
	
	@Test
	public void planetShouldContainsObstacleAt47() throws MarsRoverException {
		// Assert
		assertTrue(rover.planetContainsObstacleAt(4,7));
	}
	
	@Test
	public void planetShouldNotContainsObstacleAt11() throws MarsRoverException {
		// Assert
		assertFalse(rover.planetContainsObstacleAt(1,1));
	}
	
	@Test
	public void outputShouldBeLandingStatus() throws MarsRoverException {
		// Assert
		assertEquals("(0,0,N)",rover.executeCommand(""));
	}
	
	@Test
	public void roverShouldTurnRight() throws MarsRoverException {
		// Assert
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	public void roverShouldMoveForward() throws MarsRoverException {
		// Assert
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	
	@Test
	public void roverShouldMoveBackward() throws MarsRoverException {
		// Rover status (1,1,W)
		rover.executeCommand("f");
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("l");
		rover.executeCommand("l");
		// Assert
		assertEquals("(2,1,W)",rover.executeCommand("b"));
	}
	
	@Test
	public void roverShouldBeAt22E() throws MarsRoverException {
		// Assert
		assertEquals("(2,2,E)",rover.executeCommand("ffrff"));
	}
	
	@Test
	public void roverShouldMoveAtTheOppositeEdge() throws MarsRoverException {
		// Assert
		assertEquals("(0,9,N)",rover.executeCommand("b"));
	}

	@Test
	public void roverShouldEncounterAnObstacleOn22() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		rover = new MarsRover(10,10,planetObstacles);
		// Assert
		assertEquals("(1,2,E)(2,2)",rover.executeCommand("ffrfff"));
	}
	
	@Test
	public void roverShouldEncounterMultipleObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		rover = new MarsRover(10,10,planetObstacles);
		// Assert
		assertEquals("(1,1,E)(2,2)(2,1)",rover.executeCommand("ffrfffrflff"));
	}
	
	@Test
	public void roverShouldEncounterAnObstacleMovingAtTheOppositeEdge() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		rover = new MarsRover(10,10,planetObstacles);
		// Assert
		assertEquals("(0,0,N)(0,9)",rover.executeCommand("b"));
	}
	
	@Test
	public void roverShouldTakeATour() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		rover = new MarsRover(6,6,planetObstacles);
		// Assert
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)",rover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
}
