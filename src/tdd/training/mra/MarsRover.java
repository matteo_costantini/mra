package tdd.training.mra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MarsRover {

	private int planetX;
	private int planetY;
	private List<Cell> planetObstacles = null;
	private Set<Cell> encounteredObstacles = null;
	
	private int roverX;
	private int roverY;
	private char roverDirection;
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = new ArrayList<>();
		this.encounteredObstacles = new LinkedHashSet<>();
		
		this.roverX=0;
		this.roverY=0;
		this.roverDirection='N';
		
		initObstacles(planetObstacles);
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		for(Cell cell : planetObstacles) {
			Cell temp = new Cell(x,y);
			if(cell.equals(temp))
				return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		for(int i=0; i<commandString.length(); i++) {
			String command = commandString.substring(i,i+1);
			executeSingleCommand(command);
			int tempX = this.roverX;
			int tempY = this.roverY;
			updateOppositeEdges();
			updateEncounteredObstacles(tempX,tempY);
			updateOppositeEdges();
			System.out.println(roverStatus());
		}

		String output = roverStatus();
		for(Cell obstacle : this.encounteredObstacles)
			output += obstacle.toString();
		return output;
	}

	private void initObstacles(List<String> planetObstacles) {
		for(String s : planetObstacles) {
			s = s.substring(1,s.length()-1);
			List<String> obstacle = Arrays.asList(s.split(","));
			int cellX = Integer.parseInt(obstacle.get(0));
			int cellY = Integer.parseInt(obstacle.get(1));
			Cell cell = new Cell(cellX,cellY);
			this.planetObstacles.add(cell);
		}
	}

	private String roverStatus() {
		String status = "(x,y,z)";
		status = status.replaceAll("x",Integer.toString(roverX));
		status = status.replaceAll("y",Integer.toString(roverY));
		status = status.replaceAll("z",Character.toString(roverDirection));
		return status;
	}
	
	private void moveForward() {
		switch(this.roverDirection) {
			case 'N':
				this.roverY++;
				break;
			case 'S':
				this.roverY--;
				break;
			case 'W':
				this.roverX--;
				break;
			case 'E':
				this.roverX++;
				break;
		}
	}
	
	private void moveBackward() {
		switch(this.roverDirection) {
			case 'N':
				this.roverY--;
				break;
			case 'S':
				this.roverY++;
				break;
			case 'W':
				this.roverX++;
				break;
			case 'E':
				this.roverX--;
				break;
		}
	}
	
	private void executeSingleCommand(String command) {
		if(command.equals("r"))
			turnRight();
		if(command.equals("l"))
			turnLeft();
		if(command.equals("f"))
			moveForward();
		if(command.equals("b"))
			moveBackward();
	}
	
	private void turnRight() {
		if(this.roverDirection == 'N') {
			this.roverDirection = 'E';
			return;
		}
		if(this.roverDirection == 'E') {
			this.roverDirection = 'S';
			return;
		}
		if(this.roverDirection == 'S') {
			this.roverDirection = 'W';
			return;
		}
		if(this.roverDirection == 'W') {
			this.roverDirection = 'N';
			return;
		}
	}
	
	private void turnLeft() {
		if(this.roverDirection == 'N') {
			this.roverDirection = 'W';
			return;
		}
		if(this.roverDirection == 'W') {
			this.roverDirection = 'S';
			return;
		}
		if(this.roverDirection == 'S') {
			this.roverDirection = 'E';
			return;
		}
		if(this.roverDirection == 'E') {
			this.roverDirection = 'N';
			return;
		}
	}

	private void updateOppositeEdges() {
		if(this.roverX == -1)
			this.roverX = planetX-1;
		if(this.roverY == -1)
			this.roverY = planetY-1;
		if(this.roverX == (planetX))
			this.roverX = 0;
		if(this.roverY == (planetY))
			this.roverY = 0;
	}
	
	private void updateEncounteredObstacles(int previousX, int previousY) throws MarsRoverException {
		if(planetContainsObstacleAt(this.roverX,this.roverY)) {
			this.encounteredObstacles.add(new Cell(this.roverX,this.roverY));
			if(roverX == (this.planetX-1) && previousX == -1)
				roverX = 0;
			else if(roverY == (this.planetY-1) && previousY == -1)
				roverY = 0;
			else
				moveBackward();
		}
	}
}
